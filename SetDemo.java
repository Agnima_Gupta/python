package com.hexaware.package3;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetDemo {

	public static void main(String[] args) {
		Set<Account> account = new HashSet<>();
		account.add(new Account("Shweta",1,20000));
		account.add(new Account("Shwet",2,20000));
		account.add(new Account("Shwe",3,20000));
		account.add(new Account("Shw",4,20000));
		Iterator <Account> it = account.iterator();
		while(it.hasNext()) {
			System.out.println(it.next());
		}
		System.out.println(account.size());
		
	}

}