package com.hexaware.package3;

public interface EmployeeDAO {
	    
	    public void deposit(int amt);	    
	    public void deposit(int amt, String S);	    
	    public double withdraw(int amt);
	    public double withdraw(int amt,int S);
	    public double showbal();
	    public String getName();
	    public long getAccId();
	    

}
