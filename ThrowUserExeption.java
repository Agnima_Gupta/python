package com.hexaware.package3;

import java.util.*;
public class ThrowUserExeption {
	private int age;
	static void eligible(int age) throws InvalidAgeException{
		if(age<18) {
			throw new InvalidAgeException("Invalid Age of the user");
		}
		else {
			System.out.println("Eligible");
		}
	}
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int a=sc.nextInt();
		eligible(a);
		/*try {
			eligible(a);
		}
		catch(InvalidAgeException e) {
			System.out.println(e.getMessage()+e);
		}*/
		System.out.println();
	}
}
