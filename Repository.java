package com.hexaware.package3;
import java.util.List;

public interface Repository {
	public List<BankAccount> addAccount(BankAccount obj);
	public List<BankAccount> deleteAccount(List<BankAccount> bn, int id); 
	public void listAccounts(List<BankAccount> bn);
}
