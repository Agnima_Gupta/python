package com.hexaware.package3;

import java.util.ArrayList;
import java.util.List;

public class Client{
	

	public static void main(String[] args) {
		BankAccount ob1 = new BankAccount(1000,"Janvi",2000);
		BankAccount ob2 = new BankAccount(2000,"Shweta", 1000);
		BankAccount ob3 = new BankAccount(3000,"Mansi", 1200);
		
		RepositoryImplementation obj=new RepositoryImplementation();
		List<BankAccount> list= new ArrayList<>();		
		list=obj.addAccount(ob1);
		list=obj.addAccount(ob2);
		list=obj.addAccount(ob3);
		
		obj.listAccounts(list);
		obj.deleteAccount(list, 1000);
		obj.listAccounts(list);
		

	}
}

	