package com.hexaware.package3;


public class Account {
	public Account(String name, double accountbal, long acc_Id) {
		super();
		this.name = name;
		this.accountbal = accountbal;
		this.acc_Id = acc_Id;
	}
	private String name;
    private double accountbal;
    private long acc_Id;
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (acc_Id ^ (acc_Id >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (acc_Id != other.acc_Id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	public double getAccountbal() {
		return accountbal;
	}
	public void setAccountbal(double accountbal) {
		this.accountbal = accountbal;
	}
	@Override
	public String toString() {
		return "Account [name=" + name + ", accountbal=" + accountbal + ", acc_Id=" + acc_Id + "]";
	}
    

}
