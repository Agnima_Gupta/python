package com.hexaware.package3;
import java.io.*;

public class ReadFileDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		File file= new File("D:\\city.txt");
		BufferedReader br=null;
		try {
			br=new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(br != null) {
			String line=null;
			boolean flag=true;
			try {
				while(flag){
					line=br.readLine();
					if(line==null) {
						flag=false;
					}
					else {
						System.out.println("City is "+line);
					}
				}
			}catch(IOException e) {
				e.printStackTrace();
			}
			finally {
				if(br!= null) {
					try {
						br.close();
					}catch(Exception e) {
						e.printStackTrace();
					}
				}
			}
			
		}

	}

}
