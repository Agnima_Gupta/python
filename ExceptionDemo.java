package com.hexaware.package3;
import java.util.Scanner;


public class ExceptionDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Positive no.");
		int divident = sc.nextInt();
		
		System.out.println("Enter divisor");
		int divisor= sc.nextInt();
		float result=0;
		try {
			result=divident/divisor;
			
		}
		catch(ArithmeticException e) {
			System.out.println(e+"\n Please enter a value greater than 0");
		}
		if(result>0) {
		System.out.println("Result= "+ result);
	
		}
		
		//System.out.println("Result= "+ (divident/divisor));
		
		sc.close();
		

	}

}
