package com.hexaware.package3;
import java.util.List;
import java.util.ArrayList;

//import org.omg.CORBA.PUBLIC_MEMBER;

public class ArrayListDemo {

	public static void main(String[] args) {
		List <Integer> list= new ArrayList<>();
		
		list.add(34);
		list.add(12);
		list.add(8);
		list.add(25);
		list.add(15);
		System.out.println(list);
		
		Integer value= list.get(2);
		System.out.println("Value at index 2 is"+ value);
		
		boolean contains = list.contains(12);
		System.out.println("List contains 12");
		
		list.clear();
		
		System.out.println("Size of list:: " + list.size());
		
		
	}

}
