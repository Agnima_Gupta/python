package com.hexaware.package3;

public class InvalidAgeException extends RuntimeException{
	public InvalidAgeException(String message) {
		super(message);
	}

}
