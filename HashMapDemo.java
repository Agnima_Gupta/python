package com.hexaware.package3;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class HashMapDemo {

	public static void main(String[] args) {

		BankAccount ob1 = new BankAccount(1000, "Janvi", 2000);
		BankAccount ob2 = new BankAccount(1001, "Shweta", 1000);
		BankAccount ob3 = new BankAccount(1002, "Mansi", 1200);
		BankAccount ob4 = new BankAccount(1003, "ShwetaNeha", 18000);
		BankAccount ob5 = new BankAccount(1004, "AntickShrija", 12000);
		Map<Long, BankAccount> map = new HashMap<>();

		map.put(1000L, ob1);
		map.put(1001L, ob2);
		map.put(1002L, ob3);
		map.put(1003L, ob4);
		map.put(1004L, ob5);
		// BankAccount bankAccount = map.get(1004L);

		// System.out.println("Size of the hashMap is : " + map.size());
		// System.out.println("Bank account object for id 1004 is : " + bankAccount);
//		Set<Long> keys = map.keySet();
//		Iterator<Long> it= keys.iterator();
//		while(it.hasNext()) {
//			System.out.println(map.get(it.next()));
//		
		Set<Map.Entry<Long, BankAccount>> entrysSet = map.entrySet();
		Iterator<Map.Entry<Long, BankAccount>> it = entrysSet.iterator();

		while (it.hasNext()) {
			Map.Entry<Long, BankAccount> entry = it.next();
			System.out.printf("%s%s%n", entry.getKey(), entry.getValue());
		}
	}
}
