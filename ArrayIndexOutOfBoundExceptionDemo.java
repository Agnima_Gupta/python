package com.hexaware.package3;
import java.util.Scanner;
public class ArrayIndexOutOfBoundExceptionDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//int a[]= {1, 2 ,3 ,4 ,5 ,6 ,7 ,8 ,9 ,0};
		int arr[] = null;
		Scanner sc=new Scanner(System.in);
		
		System.out.println("Enter Index you want to access:\t");
		int index=sc.nextInt();
	
		try {
			int res=arr[index-1];
			System.out.println("The value at index "+index+" is "+ res);
		}
		catch(NullPointerException e) {
			System.out.println("The array canot be initialized with null pointer");
		}
		catch(ArrayIndexOutOfBoundsException e) {
			System.out.println("Enter the index from 1 to "+(arr.length));
		}
		
	}

}
